#This is the main API for Redis For Python Concurrency

from rq import Queue
from worker import conn
import os
q = Queue(connection=conn)
import time

def test_one(secs):
    time.sleep(secs)
    return "{} seconds took for return".format (secs)

from extensions.redis.conn import r, job
import requests

@job('default', connection=r, timeout=5)
def count_words_at_url(url):
    resp = requests.get(url)
    return len(resp.text.split())

